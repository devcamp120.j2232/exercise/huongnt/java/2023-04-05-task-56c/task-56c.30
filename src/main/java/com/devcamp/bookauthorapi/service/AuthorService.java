package com.devcamp.bookauthorapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapi.models.Author;

@Service
public class AuthorService {

    Author author1 = new Author("Margaret Mitchell", "Mitchell@gmail.com", 'f');
    Author author2 = new Author("William", "w@gmail.com", 'm');
    Author author3 = new Author("Conan Doyle", "nothing@gmail.com", 'm');
    Author author4 = new Author("Agatha Christie", "a@gmail.com", 'f');
    Author author5 = new Author("Nguyen Du", "d@gmail.com", 'm');
    Author author6 = new Author("Nam Cao", "c@gmail.com", 'm');

    public ArrayList<Author> getAuthorlist1() {
        ArrayList<Author> novelList = new ArrayList<>();
        novelList.add(author1);
        novelList.add(author2);
        return novelList;
    }

    public ArrayList<Author> getAuthorlist2() {
        ArrayList<Author> detectivelList = new ArrayList<>();
        detectivelList.add(author3);
        detectivelList.add(author4);
        return detectivelList;
    }

    public ArrayList<Author> getAuthorlist3() {
        ArrayList<Author> vnNovelList = new ArrayList<>();
        vnNovelList.add(author5);
        vnNovelList.add(author6);
        return vnNovelList;
    }

    // get all authors
    public Author getAuthorByEmail(String email) {
        ArrayList<Author> allAuthors = new ArrayList<>();
        allAuthors.add(author1);
        allAuthors.add(author2);
        allAuthors.add(author3);
        allAuthors.add(author4);
        allAuthors.add(author5);
        allAuthors.add(author6);

        Author findAuthor = new Author();
        for (Author authorElement : allAuthors) {
            if (authorElement.getEmail().equalsIgnoreCase(email)) {
                findAuthor = authorElement;

            }
        }

        return findAuthor;
    }

    public ArrayList<Author> getAuthorByGender(char gender) {
        ArrayList<Author> allAuthors = new ArrayList<>();
        allAuthors.add(author1);
        allAuthors.add(author2);
        allAuthors.add(author3);
        allAuthors.add(author4);
        allAuthors.add(author5);
        allAuthors.add(author6);
        
        ArrayList<Author> findAuthorbyGender = new ArrayList<>();
        for (int i = 0; i < allAuthors.size(); i++) {
            Author s = allAuthors.get(i);
            if (s.getGender() == gender) {
                findAuthorbyGender.add(s);
            }
        }
        return findAuthorbyGender;

    }
}
