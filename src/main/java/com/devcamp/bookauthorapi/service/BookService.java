package com.devcamp.bookauthorapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapi.models.Book;

@Service
public class BookService {
@Autowired
    public AuthorService authorService = new AuthorService();

    Book book1 = new Book("Novel", authorService.getAuthorlist1(), 200000.0,1);
    Book book2 = new Book("Detective novel", authorService.getAuthorlist2(), 200000.0,2);
    Book book3 = new Book("Viet Nam novel", authorService.getAuthorlist3(), 200000.0,3);

    //get all books
    public ArrayList<Book> getAllBooks() {
        ArrayList<Book> allBooks = new ArrayList<>();
        allBooks.add(book1);
        allBooks.add(book2);
        allBooks.add(book3);
        return allBooks;
    }

    //get books by quantityNumber
    
    public ArrayList<Book> getBookByCode( int qty) {
        ArrayList<Book> books = new ArrayList<>();
        for (int i=0; i< getAllBooks().size(); i++){
            if (getAllBooks().get(i).getQty() >= qty){
                books.add(getAllBooks().get(i));
            }
        }
        return books;

    }
}
