package com.devcamp.bookauthorapi.Controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.models.Book;
import com.devcamp.bookauthorapi.service.BookService;

@RestController
@CrossOrigin
@RequestMapping("/api")

public class BookController {
    @Autowired

    public BookService bookService;
    @GetMapping("/books")

    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> allBookList = bookService.getAllBooks();
        return allBookList;
    }

    @GetMapping("/book-quantity")
    public ArrayList<Book> getBookQuantity(@RequestParam (name="qty", required = true)int qty){
        ArrayList<Book> bookQuantityList = bookService.getBookByCode(qty);
        return bookQuantityList;

    }
}
